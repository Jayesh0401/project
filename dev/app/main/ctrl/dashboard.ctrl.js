/**
 * @namespace act.Main
 */

define(['angular-chart'], function(chartjs) {

  angular
    .module('act.Main')
    .controller('MainDashboardController', controller);

  controller.$inject = ['$scope', '$rootScope'];

  return controller;

  // //////////////////////////////////////////////////////

  /**
   * Main Dashboard Controller
   *
   * @public
   *
   * @memberof   act.Main
   *
   * @author     jayeshactonate
   */

  function controller($scope, $rootScope) {
    $rootScope.pageTitle = 'Dashboard';

     $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
     $scope.data = [100, 500, 300];
    var config = {
        type: 'bar',
        data: {
            datasets: [{
                data: [],
                label: 'Booking'
            }],
            labels: []
        },
        options: {
            responsive: true,
            animation: {
                animateScale: true,
                animateRotate: true
            },
             scales: {
                  yAxes: [
                  {
                    scaleLabel: {
                      display: true,
                      labelString: 'Revenue'
                    }
                  }]
              }
        }
    };
    var ctx = document.getElementById("doughnut").getContext("2d");
    window.myDoughnut = new Chart(ctx, config);
    var  i =0;
    for( i; i<=$scope.data.length; i++) {
      myDoughnut.config.data.labels.push("Booking");
      myDoughnut.config.data.datasets[0].data.push($scope.data[i]);
    }
    if(i>=$scope.data.length) {
      myDoughnut.config.data.datasets[0].data.push(0);
    }
   // var chart = new Chart('chartjs');
   var ViewModel = this;
    
  }
})