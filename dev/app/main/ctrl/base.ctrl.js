/**
 * @namespace act.Main
 */
define([
  'app/auth/services/auth.service'
  ], function() {

  angular
    .module('act.Main')
    .controller('MainBaseController', controller);

  controller.$inject = ['$state', '$scope','$mdSidenav', 'logger', 'AuthService', 'ToastService', '$rootScope','ssSideNav', '$timeout'];

  return controller;

  // //////////////////////////////////////////////////////

  /**
   * Main Base Controller
   *
   * @public
   *
   * @memberof   act.Main
   *
   * @author     jayeshactonate
   *
   * @param      {Object}    $state        ui-router state service
   * @param      {Function}  $mdSidenav    Angular Material Sidenav
   * @param      {Object}    logger        act.log logger
   * @param      {Object}    AuthService   act.Auth.AuthService
   * @param      {Object}    ToastService  act.Services.ToastService
   */
  function controller($state, $scope,$mdSidenav, logger, AuthService, ToastService, $rootScope,ssSideNav,$timeout) {
    $rootScope.pageTitle = 'Dashboard';
    $rootScope.TeamOption = false;
    $scope.menu = ssSideNav;
    var ViewModel = this;
    var sideNavId = 'mainSidenav';
    ViewModel.toggleSidenav = toggleSidenav;
    ViewModel.logoutUser = logoutUser;
    
    ViewModel.permission = $rootScope.userDetails;

   
    // /////////////////////////////////////////////////////
    $rootScope.ShowTeamOptions = _ShowTeamOptions;


    function _ShowTeamOptions() {
      if($rootScope.TeamOption == false) {
        $rootScope.TeamOption = true;
      } else {
        $rootScope.TeamOption = false;
      }
    }

    function toggleSidenav() {
      var data = $mdSidenav(sideNavId).toggle();
    }

    function logoutUser() {
      ToastService.loading();

      AuthService.logout()
        .then(function(res) {
          ToastService.hide();
          $state.go('app.auth.login');
        })
        .catch(function(err) {
          ToastService.error(err.message);
        });
    }



    // Show or Hide menu
    ssSideNav.setVisible('link_1');
    ssSideNav.setVisibleFor([{
      id: 'toggle_item_1',
      value: true
    }, {
      id: 'link_1',
      value: false
    }]);

    $timeout(function () {
      ssSideNav.setVisible('toogle_2', false);
    });

    $timeout(function () {
      // force selection on child dropdown menu item and select its state too.
      ssSideNav.forceSelectionWithId('toogle_1_link_2');
    }, 1000 * 3);
  }

});

