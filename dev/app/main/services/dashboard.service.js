define([
  'app-bootstrap'
],function(){
  
  angular.module('act.Main')
  .factory('DashboardService', DashboardService);
  
  DashboardService.$inject = ['logger', 'Rest', '$q', '$timeout'];
  
  function DashboardService(logger, Rest, $q, $timeout) {
    var log = logger.log().child('DashboardService');
    console.log('I am here in Dashboard Service');
    return {
      getBooking: getBooking
    };
    /**
     * Gets the booking.
     *
     * @public
     *
     * @memberof   Dashboard Service
     *
     * @author     jayesh
     *
     * @param      {object}  value1  The value 1
     * @param      {object}  value2  The value 2
     * @return     {return}  The booking.
     */
    function getBooking(value1, value2) {
      var deferred = $q.defer();
      var httpResource = Rest.resource('bookings').post('get');
      httpResource({'value1': value1, 'value2': value2}).then(function(response){
        deferred.resolve(response);
      }).catch(function(error) {
        deferred.reject(error);
      }
      );
      return deferred.promise;
    }

  }
});
