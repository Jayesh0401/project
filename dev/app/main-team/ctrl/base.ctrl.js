/**
 * @namespace act.Main
 */
define([
    'app/main-team/services/teams.services',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.Teams')
        .controller('MainTeamsBaseController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'TeamsService', '$mdDialog', 'DialogService', '$rootScope', '$scope'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  TeamsService  act.Main.Teams.TeamsService
     */
    function controller($state, logger, ToastService, TeamsService, $mdDialog, DialogService, $rootScope, $scope) {
        $rootScope.pageTitle = 'Manage Teams';

        var ViewModel = this;

        // Parameter Declaration
        ViewModel.teamleaders = [];
        ViewModel.users = [];
        $scope.teamMembers = [];
        ViewModel.ShowButton = false;

        // Function Declarations
        ViewModel.GetTeamLeaders = _GetTeamLeaders;
        ViewModel.SelectedTeamLeader = _SelectedTeamLeader;
        ViewModel.createTeam = _createTeam;
        $scope.teamMembersSelection = _teamMembersSelection;

        // calling function on page load
        _GetTeamLeaders();
        /**
         * Gets the team leaders.
         */
        function _GetTeamLeaders() {
            TeamsService.GetTeamLeaders({}).then(function(result) {
                ViewModel.teamleaders = result.data.data;
            });
        };

        /**
         * Selects Team Leader
         *
         * @param      {string}  teamLeader  The team leader
         */
        function _SelectedTeamLeader(teamLeader) {
            $scope.teamMembers =[];
            if (teamLeader !== '-1') {
                ViewModel.SelectedTeamLeaderId = teamLeader;
                var teamLeaderId = {
                  id: teamLeader
                }
                TeamsService.getUsers().then(function(response) {
                    ViewModel.users = response.data.data;
                    return TeamsService.GetAllTeamMembers(teamLeaderId);
                }).then(function(result) {
                    if(result.data.success && result.data.data[0].teamMemberId.length !== 0) {
                        for(i=0;i<result.data.data[0].teamMemberId.length; i++) {
                          $scope.teamMembers.push(result.data.data[0].teamMemberId[i].id);
                          ViewModel.ShowButton = true;
                        }      
                    }
                // response.data.data[0].teamMemberId
                // console.log(result.data.data[0].teamMemberId, '::::::::::::::::::;teamMembers');
                  
                });
            }
        };

        /**
         * Team Members Selected
         *
         * @param      {<type>}  id      The identifier
         */
        function _teamMembersSelection(id) {
            var idx = $scope.teamMembers.indexOf(id);
            if (idx > -1) {
                $scope.teamMembers.splice(idx, 1);
                if ($scope.teamMembers.length == 0) {
                    ViewModel.ShowButton = false;
                }
            } else {
                ViewModel.ShowButton = true;
                $scope.teamMembers.push(id);
            }
        };

        /**
         * Creates a team.
         */
        function _createTeam() {
            var data = {
                teamleader: ViewModel.SelectedTeamLeaderId,
                teamMembers: $scope.teamMembers
            }
            TeamsService.createTeam(data).then(function(response) {
                if(response.data && response.data.success) {
                    ToastService.success(response.data.message);
                }
            });
        }
    }
});