/**
 * @namespace act.Main
 */
define([
  'app/main-team/services/teams.services',
   'app/services/dialog.service'
  ], function() {

  angular
    .module('act.Main.Teams')
    .controller('MainTeamController', controller);

  controller.$inject = ['$state', 'logger', 'ToastService', 'TeamsService','$mdDialog', 'DialogService', '$rootScope', '$scope'];

  return controller;

  // //////////////////////////////////////////////////////

  /**
   * Main Base Controller
   *
   * @public
   *
   * @memberof   act.Main
   *
   * @author     jayeshactonate
   *
   * @param      {Object}  $state        ui-router state service
   * @param      {Object}  logger        act.log logger
   * @param      {Object}  ToastService  act.Services.ToastService
   * @param      {Object}  TeamsService  act.Main.Teams.TeamsService
   */
  function controller($state, logger, ToastService, TeamsService, $mdDialog, DialogService, $rootScope,$scope) {
    $rootScope.pageTitle = 'View Teams';
      
    var ViewModel = this;
    // Parameter Declaration
    ViewModel.users = [];

    // function Declaration
    ViewModel.SelectedTeamLeader = _SelectedTeamLeader;
    ViewModel.GetTeamLeaders = _GetTeamLeaders;
    


    // calling function on page load
    _GetTeamLeaders();
    /**
     * Gets the team leaders.
     */
    function _GetTeamLeaders() {
       TeamsService.GetTeamLeaders({}).then(function(result) {
        ViewModel.teamleaders = result.data.data;
      });
    };

    /**
     * { function_description }
     *
     * @param      {id}  id      The identifier
     */
    function _SelectedTeamLeader(id) {
      ViewModel.users = [];
      if(id !== '-1') {
         var teamleaderId = {
          id:id
         }
          TeamsService.GetAllTeamMembers(teamleaderId).then(function(response) {
            if(response.data &&  response.data.data[0].teamMemberId.length !== 0 ) {
              for(i = 0; i<response.data.data[0].teamMemberId.length; i++) {
                ViewModel.users.push(response.data.data[0].teamMemberId[i]);
              } 
            } else {
                ToastService.error('Selected Team Leader Does not have any team members');
            }
          }).catch(function(error) {
              ViewModel.users = [];
              console.log(error);
          });
      }
    }
  }
});