/**
 * @namespace act.Users
 */
define([
 'app-bootstrap'
], function() {

  angular
    .module('act.Main.Teams')
    .config(RouterConfig);


  // /////////////////////////////////////

  /*=====================================
  =            Config Blocks            =
  =====================================*/

  RouterConfig.$inject = ['APP_BASE_PATH', '$stateProvider', 'lazyProvider'];

  /**
   * Configure the act.Main.Users module's routes
   *
   * @public
   *
   * @memberof   act.Main
   *
   * @author     jayeshactonate
   *
   * @class
   * @param      {String}  APP_BASE_PATH   App Base path
   * @param      {Object}  $stateProvider  ui-router's stateProvider which is used to create
   *                                       application states/routes
   * @param      {Object}  lazyProvider    Provider instance of act.lazy used to lazy load modules
   */
  function RouterConfig(APP_BASE_PATH, $stateProvider, lazyProvider) {

    var BASE_DIR_CTRL = APP_BASE_PATH + 'app/main-team/ctrl/';
    var BASE_DIR_TPL = APP_BASE_PATH + 'app/main-team/tpl/';

    lazyProvider.configure('act.Main.Teams');

    $stateProvider
      .state('app.main.manageteams', {
        url: '/manage/teams',
        templateUrl: BASE_DIR_TPL + 'base.tpl.html',
        controller: 'MainTeamsBaseController',
        controllerAs: 'ViewModel',
        resolve: {
          services: ['lazy',
            function(lazy) {
              return lazy.load(BASE_DIR_CTRL + 'base.ctrl.js');
            }
          ]
        }
      }).state('app.main.viewteams', {
        url: '/view/teams',
        templateUrl: BASE_DIR_TPL + 'ViewTeam.tpl.html',
        controller: 'MainTeamController',
        controllerAs: 'ViewModel',
        resolve: {
          services: ['lazy',
            function(lazy) {
              return lazy.load(BASE_DIR_CTRL + 'team.ctrl.js');
            }
          ]
        }
      });
  }

});
