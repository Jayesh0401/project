define([
	'app-bootstrap'
],function(){
	
	angular.module('act.Main.Teams')
	.factory('TeamsService', TeamsService);
	
  TeamsService.$inject = ['logger', 'Rest', '$q', '$timeout'];
  
  function TeamsService(logger, Rest, $q, $timeout) {
    var log = logger.log().child('TeamsService');

    return {
    	GetTeamLeaders: GetTeamLeaders,
      getUsers: getUsers,
      createTeam:createTeam,
      GetTeamMembers: GetTeamMembers,
      GetAllTeamMembers: GetAllTeamMembers
    };


    /**
     * Gets the team leaders.
     *
     * @class      GetTeamLeaders (name)
     * @return     {return}  The team leaders.
     */
    function GetTeamLeaders() {
    var deferred = $q.defer();
    var httpResource = Rest.resource('get/team').post('leaders');
    httpResource().then(function(result) {
      deferred.resolve(result);
    }).catch(function(error) {
      deferred.reject();
    }
    );
      return deferred.promise;
    }

    /**
     * Gets the users.
     *
     * @return     {return}  The users.
     */
    function getUsers() {
      var deferred = $q.defer();
      var httpResource = Rest.resource('get/users').post('only');
      httpResource().then(function(result) {
        deferred.resolve(result);
      }).catch(function(error) {
        deferred.reject();
      }
      );
      return deferred.promise;
    
    }
    /**
     * Creates a team.
     *
     * @param      {data}  data    The data
     * @return     {return}  { description_of_the_return_value }
     */
    function createTeam(data) {
      var deferred = $q.defer();
      var httpResource = Rest.resource('create').post('team');
      httpResource(data).then(function(result) {
        deferred.resolve(result);
      }).catch(function(error) {
        deferred.reject();
      }
      );
      return deferred.promise;
    }
    /**
     * Gets the team members.
     *
     * @class      GetTeamMembers (name)
     * @param      {id}  id      The identifier
     * @return     {return}  The team members.
     */
    function GetTeamMembers(id) {
        var deferred = $q.defer();
      var httpResource = Rest.resource('get/team').post('members');
      httpResource(id).then(function(result) {
        deferred.resolve(result);
      }).catch(function(error) {
        deferred.reject();
      }
      );
      return deferred.promise;
    }
    /**
     * Gets all team members.
     *
     * @class      GetAllTeamMembers (name)
     * @param      {id}  id      The identifier
     * @return     {return}  All team members.
     */
    function GetAllTeamMembers(id) {
          var deferred = $q.defer();
          var teamLeaderid = id;
          var httpResource = Rest.resource('get/all/team').post('members');
          httpResource(teamLeaderid).then(function(result) {
            deferred.resolve(result);
          }).catch(function(error) {
            deferred.reject();
          }
          );
          return deferred.promise;
      }
  }
});