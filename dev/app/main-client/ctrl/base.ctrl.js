/**
 * @namespace act.Main
 */
define([
    'app/main-clients/services/clients.service',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.Clients')
        .controller('MainClientsBaseController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'UsersService', '$mdDialog', 'DialogService', '$rootScope'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  UsersService  act.Main.Users.UsersService
     */
    function controller($state, logger, ToastService, UsersService, $mdDialog, DialogService, $rootScope) {
        $rootScope.pageTitle = 'Users';

        var ViewModel = this;
        ViewModel.query = {
            order: 'name',
            limit: 5,
            page: 1
        };
        
        ViewModel.selected = [];
        ViewModel.users = [];
        ViewModel.loadPromise = false;
        ViewModel.NewUser = NewUser;
        ViewModel.EditUser = EditUser;

        getUsers();
        var PermPermissionStrategies = {
            showElement: function($element) {
                $element.removeClass('ng-hide');
            },
            hideElement: function($element) {
                $element.addClass('ng-hide');
            }
        };
        ViewModel.permission = $rootScope.userDetails;

        /**
         * Gets the users.
         *
         * @public
         *
         * @memberof   User Base Controller
         *
         * @author     jayesh
         */
        function getUsers() {
            UsersService.getUsers().then(function(result) {
                ViewModel.users = result.data.data;
            });
        }
        /**
         * Deactivate  User
         *
         * @public
         *
         * @memberof   User Base Controller
         *
         * @author     jayesh
         *
         * @class      DeactivateUser (name)
         * @param      {object}  userid  The userid
         */
        ViewModel.DeactivateUser = function DeactivateUser(userid) {
            UsersService.DeactivateUser(userid).then(function(response) {
                ToastService.success(response.data.message);
            });
        }
        /**
         * Deletes The User
         *
         * @public
         *
         * @memberof   User Base Controller
         *
         * @author     jayesh
         *
         * @class      DeleteUser (name)
         * @param      {object}  userid  The userid
         */
        ViewModel.DeleteUser = function DeleteUser(userid) {
            UsersService.DeleteUser(userid).then(function(response) {
                ToastService.success(response.data.message);
                getUsers();
            });
        }

        function NewUser() {
          $state.go('app.main.NewUser');
        }

        function EditUser(id) {
          $state.go('app.main.EditUser', {id:id});
        }
    }
    // New User Controller Ends
});