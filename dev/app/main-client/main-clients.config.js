/**
 * @namespace act.Users
 */
define([
 'app-bootstrap'
], function() {

  angular
    .module('act.Main.Clients')
    .config(RouterConfig);


  // /////////////////////////////////////

  /*=====================================
  =            Config Blocks            =
  =====================================*/

  RouterConfig.$inject = ['APP_BASE_PATH', '$stateProvider', 'lazyProvider'];

  /**
   * Configure the act.Main.Users module's routes
   *
   * @public
   *
   * @memberof   act.Main
   *
   * @author     jayeshactonate
   *
   * @class
   * @param      {String}  APP_BASE_PATH   App Base path
   * @param      {Object}  $stateProvider  ui-router's stateProvider which is used to create
   *                                       application states/routes
   * @param      {Object}  lazyProvider    Provider instance of act.lazy used to lazy load modules
   */
  function RouterConfig(APP_BASE_PATH, $stateProvider, lazyProvider) {

    var BASE_DIR_CTRL = APP_BASE_PATH + 'app/main-clients/ctrl/';
    var BASE_DIR_TPL = APP_BASE_PATH + 'app/main-clients/tpl/';

    lazyProvider.configure('act.Main.Clients');

    $stateProvider
      .state('app.main.clients', {
        url: '/clients',
        templateUrl: BASE_DIR_TPL + 'base.tpl.html',
        controller: 'MainClientsBaseController',
        controllerAs: 'ViewModel',
        resolve: {
          services: ['lazy',
            function(lazy) {
              return lazy.load(BASE_DIR_CTRL + 'base.ctrl.js');
            }
          ]
        }
      })
      .state('app.main.NewClient', {
        url: '/new/client',
        templateUrl: BASE_DIR_TPL + 'new_Client.tpl.html',
        controller: 'MainNewClientBaseController',
        controllerAs: 'ViewModel',
        resolve: {
          services: ['lazy',
            function(lazy) {
              return lazy.load(BASE_DIR_CTRL + 'new.ctrl.js');
            }
          ]
        }
      })
      .state('app.main.EditClient', {
        url: '/edit/client/:id',
        templateUrl: BASE_DIR_TPL + 'edit_Client.tpl.html',
        controller: 'MainEditClientBaseController',
        controllerAs: 'ViewModel',
        resolve: {
          services: ['lazy',
            function(lazy) {
              return lazy.load(BASE_DIR_CTRL + 'edit.ctrl.js');
            }
          ]
        }
      });
  }

});
