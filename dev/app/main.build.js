/**
 * @namespace act
 */
define([
    // Base Config & Req
    'app-bootstrap',

    // Modules Configs
    'app/base/base.config',
    'app/auth/auth.config',
    'app/main/main.config',
    'app/main-users/main-users.config',
    'app/main-team/main-team.config',
    'app/main-project/main-project.config',
    'app/main-project-task/main-project-task.config',
    'app/main-my-task/main-my-task.config',
], function() {

    angular.module('act', [
            'act.Base',
            'act.Globals',

            // All V2 Modules
            'act.Auth',
            'act.Main',
            'act.Main.Users',
            'act.Main.Teams',
            'act.Main.Project',
            'act.Main.ProjectTask',
            'act.Main.MyTasks',
            // All 3rd party modules
            'ngMaterial',
            'ngAnimate',
            'ngAria',
            'md.data.table',
            'permission',
            'permission.ui',
            'sasrio.angular-material-sidenav'
        ])
        .config(NgMaterialConfig);

    // /////////////////////////////////////

    /*=====================================
    =            Config Blocks            =
    =====================================*/

    NgMaterialConfig.$inject = ['$mdThemingProvider', 'ssSideNavSectionsProvider', '$mdIconProvider', '$$mdSvgRegistry'];

    /**
     * Set theme colors for Angular Material
     *
     * @public
     *
     * @memberof   act
     *
     * @author     jayeshactonate
     *
     * @class      NgMaterialConfig
     * @param      {Object}  $mdThemingProvider  Angular Material Theming provider
     */
    function NgMaterialConfig($mdThemingProvider, ssSideNavSectionsProvider, $mdIconProvider, $$mdSvgRegistry) {

        $mdThemingProvider.definePalette('appPrimaryPalette', {
            '50': 'E3F2FD',
            '100': 'BBDEFB',
            '200': '90CAF9',
            '300': '64B5F6',
            '400': '42A5F5',
            '500': '2196F3',
            '600': '1E88E5',
            '700': '1976D2',
            '800': '1565C0',
            '900': '0D47A1',
            'A100': '82B1FF',
            'A200': '448AFF',
            'A400': '2979FF',
            'A700': '2962FF',
            'contrastDefaultColor': 'light',
            'contrastLightColors': ['50', '100',
                '200', '300', '400', 'A100'
            ],
        });

        $mdThemingProvider.theme('default')
            .primaryPalette('appPrimaryPalette')
            .accentPalette('blue');

        $mdThemingProvider.theme('successTheme')
            .primaryPalette('green')
            .accentPalette('blue');

        $mdIconProvider
            .icon('md-close', $$mdSvgRegistry.mdClose)
            .icon('md-menu', $$mdSvgRegistry.mdMenu)
            .icon('md-toggle-arrow', $$mdSvgRegistry.mdToggleArrow);

        ssSideNavSectionsProvider.initWithTheme($mdThemingProvider);
        ssSideNavSectionsProvider.initWithSections([{
                id: 'link_1',
                name: 'Dashboard ',
                state: 'app.main.dashboard',
                type: 'link'
            },
            {
                id: 'dashboard',
                children: [{
                    name: '𝔇𝔞𝔰𝔥𝔟𝔬𝔞𝔯𝔡',
                    type: 'toggle',
                    icon: "md-toggle-arrow",
                    pages: [{
                        id: 'dashboard',
                        name: 'Dashboard',
                        state: 'app.main.dashboard'
                    }]
                }]
            },
            {
                id: 'User',
                children: [{
                    name: '𝔘𝔰𝔢𝔯𝔰',
                    type: 'toggle',
                    icon: "md-toggle-arrow",
                    pages: [{
                        id: 'user',
                        name: 'Users',
                        state: 'app.main.users',
                    }]
                }]
            }, {
                id: 'Team',
                children: [{
                    name: '𝔗𝔢𝔞𝔪𝔰  ',
                    type: 'toggle',
                    icon: "md-toggle-arrow",
                    pages: [{
                            id: 'toogle_2_link_1',
                            name: 'Manage Team',
                            state: 'app.main.manageteams'
                        },
                        {
                            id: 'toogle_2_link_1',
                            name: 'View Team',
                            state: 'app.main.viewteams'
                        }
                    ]
                }]
            }, {
                id: 'Project',
                children: [{
                    name: '𝔓𝔯𝔬𝔧𝔢𝔠𝔱𝔰',
                    type: 'toggle',
                    icon: "md-toggle-arrow",
                    pages: [{
                            id: 'project',
                            name: 'Projects',
                            state: 'app.main.project'
                        },
                        {
                            id: 'task',
                            name: 'Task',
                            state: 'app.main.projectTask'
                        }
                    ]
                }]
            }, {
                id: 'Task',
                children: [{
                    name: '𝔓𝔯𝔬𝔧𝔢𝔠𝔱𝔰',
                    type: 'toggle',
                    icon: "md-toggle-arrow",
                    pages: [{
                        id: 'Task',
                        name: 'My Tasks',
                        state: 'app.main.myTask'
                    }]
                }]
            }
        ]);
    }

    /*=====  End of Config Blocks  ======*/

});