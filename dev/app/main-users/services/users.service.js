define([
	'app-bootstrap'
],function(){
	
	angular.module('act.Main.Users')
	.factory('UsersService', UsersService);
	
  UsersService.$inject = ['logger', 'Rest', '$q', '$timeout'];
  
  function UsersService(logger, Rest, $q, $timeout) {
    var log = logger.log().child('UsersService');

    return {
      get: get,
      getUsers: getUsers,
      DeactivateUser: DeactivateUser,
      DeleteUser: DeleteUser,
      EditUser: EditUser,
      createUser: createUser,
      findUserByid: findUserByid,
      GetAllPermission: GetAllPermission
    };

  /**
   * Gets the users.
   *
   * @public
   *
   * @memberof   Users Service
   *
   * @author     jayesh
   *
   * @return     {return}  The users.
   */
  function getUsers() {
    var deferred = $q.defer();
    var httpResource = Rest.resource('users').post('get');
    httpResource().then(function(result) {
      deferred.resolve(result);
    }).catch(function(error) {
      deferred.reject();
    }
    );
    return deferred.promise;
  }
  /**
   * Deactivate User
   *
   * @public
   *
   * @memberof   Users Service
   *
   * @author     jayesh
   *
   * @class      DeactivateUser (name)
   * @param      {object}  id      The identifier
   * @return     {return}  { description_of_the_return_value }
   */
  function DeactivateUser(id) {
    var deferred = $q.defer();
    var httpResource = Rest.resource('user').post('deactivate');
    httpResource({'id':id}).then(function(result) {
      deferred.resolve(result);
    }).catch(function(error){
      deferred.reject(error);
    }
    );
    return deferred.promise;
  }

  /**
   * Users Session
   *
   * @public
   *
   * @memberof   Users Service
   *
   * @author     jayesh
   *
   * @param      {object}  options  The options
   * @return     {return}  { description_of_the_return_value }
   */
  function get(options) {
      var deferred = $q.defer();

      var httpResource = Rest.resource('user').get('session');

      $timeout(function() {
        deferred.resolve({code: 'SUCCESS', data: []});
      }, 5000);

      // httpResource('')
      //   .then(function(res) {             
      //     log.info('Users loaded', res.data);

      //     deferred.resolve({code: 'SUCCESS', data: res.data});
      //   })
      //   .catch(function(err) {
      //     log.error('Error fetching users', err);
      //     deferred.reject({code: 'ERROR', error: err, message: 'Error loading users'});
      //   });

      return deferred.promise;
    }
    /**
     * Deletes The user
     *
     * @public
     *
     * @memberof   User Service
     *
     * @author     jayesh
     *
     * @class      DeleteUser (name)
     * @param      {object}  id      The identifier
     * @return     {return}  { description_of_the_return_value }
     */
    function DeleteUser(id) {
      var deferred = $q.defer();
      var httpResource = Rest.resource('user').post('delete');
      httpResource({'id':id}).then(function(result) {
        deferred.resolve(result);
      }).catch(function(error) {
        deferred.reject(error);
      }
      );
      return deferred.promise;
    }
    /**
     * Edit User
     *
     * @public
     *
     * @memberof   Users Service
     *
     * @author     jayesh
     *
     * @class      EditUser (name)
     * @param      {object}  userdetails  The userdetails
     * @return     {return}  { description_of_the_return_value }
     */
    function EditUser(userdetails) {
      var deferred = $q.defer();
      var httpResource = Rest.resource('user').post('update');
      httpResource({'data':userdetails}).then(function(result) {
        deferred.resolve(result);
      }).catch(function(error) {
        deferred.reject(error);
      }
      );
      return deferred.promise;
    }
    /**
     * Creates an user.
     *
     * @public
     *
     * @memberof   Users Service
     *
     * @author     jayesh
     *
     * @param      {object}  user    The user
     * @return     {object}  { description_of_the_return_value }
     */
    function createUser(user) {
      var deferred = $q.defer();
      var httpResource = Rest.resource('user').post('create');
      httpResource({'user':user}).then(function(response) {
        deferred.resolve(response);
      }).catch(function(error){
        deferred.reject(error);
      }
      );
      return deferred.promise;
    }
   /**
    * Get User By Id
    *
    * @public
    *
    * @memberof   Users Service
    *
    * @author     jayesh
    *
    * @param      {object}  userid  The userid
    * @return     {return}  { description_of_the_return_value }
    */
    function  findUserByid(userid) {
      var deferred = $q.defer();
      var httpResource = Rest.resource('user/by/id').post('get');
      httpResource({'userid': userid}).then(function(response) {
        deferred.resolve(response);
      }).catch(function(error) {
        deferred.reject(error);
      }
      );
      return deferred.promise;
    }
    /**
     * Gets all permission.
     *
     * @class      GetAllPermission (name)
     * @return     {return}  All permission.
     */
    function GetAllPermission() {
      var deferred = $q.defer();
      var httpResource = Rest.resource('users/permissions').post('get');
      httpResource().then(function(response) {
        deferred.resolve(response);
      }).catch(function(error) {
        deferred.reject(error);
      }
      );
      return deferred.promise;

    }
  }
});