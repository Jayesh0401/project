/**
 * @namespace act.Main
 */
define([
    'app/main-users/services/users.service',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.Users')
        .controller('MainEditUserBaseController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'UsersService', '$mdDialog', 'DialogService', '$rootScope', '$stateParams','$scope'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  UsersService  act.Main.Users.UsersService
     */
    function controller($state, logger, ToastService, UsersService, $mdDialog, DialogService, $rootScope, $stateParams ,$scope) {
        $rootScope.pageTitle = 'New User';
        var ViewModel = this;

        var user = $stateParams;
        

        ViewModel.user = {};
        ViewModel.closeDialog = '';
        ViewModel.user = user;
        ViewModel.ShowAdminOption = true;
        ViewModel.roleSelected = false;
        ViewModel.rolesAdmin = false;
        
        $scope.selectionPermission = [];

        if(user && user.id) {
          var id = user.id;
          UsersService.findUserByid(id).then(function(response) {
            ViewModel.user = response.data.data[0];
            setindex();
          }).catch(function(error) {
            console.log(error);
          });
        }

        // Function Declaration
        ViewModel.RolePermission = _RolePermission;
        _permission();



        /**
         * Set Index
         *
         * @public
         *
         * @memberof   User Base Controller
         *
         * @author     jayesh
         */
        function setindex() {
            for (i = 0; i < ViewModel.user.permission.length; i++) {
                $scope.selectionPermission.push(ViewModel.user.permission[i]);
            }
            if (ViewModel.user.role === 'SuperAdmin') {
                ViewModel.rolesAdmin = true;
            } else {
                ViewModel.rolesAdmin = false;

            }
            ViewModel.roleSelected = true;
        }

        /**
         * All the Permissions
         */
        function _permission() {
            UsersService.GetAllPermission().then(function(response) {
                ViewModel.permission = response.data.data;
            }).catch(function(error) {
                console.log(error);
            });
        }


        /**
         * Permission selects
         *
         * @param      {<type>}  code    The code
         */
        $scope.permissionSelection = function permissionSelection(code) {
            var idx = $scope.selectionPermission.indexOf(code);
            if (idx > -1) {
                $scope.selectionPermission.splice(idx, 1);
            } else {
                $scope.selectionPermission.push(code);
            }
        };

        /**
         * Edit user
         *
         * @public
         *
         * @memberof   User Base Controller
         *
         * @author     shoaibmerchant
         *
         * @class      EditUser (name)
         * @param      {<type>}  userdetails  The userdetails
         */
        ViewModel.EditUser = function EditUser(userdetails) {
            userdetails.permission = $scope.selectionPermission;
            UsersService.EditUser(userdetails).then(function(response) {
                $mdDialog.cancel();
                $scope.selection = '';
                ToastService.success(response.data.message);
                $state.go('app.main.users');
               
            });
        }

        if ($rootScope.UserRole !== 'SuperAdmin') {
            ViewModel.ShowAdminOption = false;
        }

        /**
         * Role changes Pemission
         *
         * @param      {string}  role    The role
         */
        function _RolePermission(role) {
            if (role !== '-1') {
                ViewModel.roleSelected = true;
                if (role == 'SuperAdmin') {
                    ViewModel.rolesAdmin = true;
                    for (i = 0; i < ViewModel.permission.User.length; i++) {
                        $scope.selectionPermission.push(ViewModel.permission.User[i].code);
                        $scope.selectionPermission.push(ViewModel.permission.dashboard[i].code);
                    }
                } else {
                    $scope.selectionPermission = ["rDashboard", "rUsers"];
                    ViewModel.rolesAdmin = false;
                }

            } else {
                $scope.selectionPermission = ["rDashboard", "rUsers"];
                ViewModel.rolesAdmin = false;
                ViewModel.roleSelected = false;
            }
        }
    }
});