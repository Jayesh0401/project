/**
 * @namespace act.Main
 */
define([
    'app/main-users/services/users.service',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.Users')
        .controller('MainNewUserBaseController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'UsersService', '$mdDialog', 'DialogService', '$rootScope', '$scope'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  UsersService  act.Main.Users.UsersService
     */
    function controller($state, logger, ToastService, UsersService, $mdDialog, DialogService, $rootScope, $scope) {
        $rootScope.pageTitle = 'New User';

        var ViewModel = this;
        ViewModel.user = {};

        ViewModel.RolePermission = _RolePermission;
        ViewModel.roleSelected = false;
        ViewModel.rolesAdmin = false;
        ViewModel.ShowAdminOption = true;
        $scope.rows = [];
        $scope.selection = [];
        $scope.selectionPermission = [];

        if ($rootScope.UserRole !== 'SuperAdmin') {
            ViewModel.ShowAdminOption = false;
        }
        // Function Declaration
        _permission();
        /**
         * All the Permissions
         */
        function _permission() {
            UsersService.GetAllPermission().then(function(response) {
                ViewModel.permission = response.data.data;
            }).catch(function(error) {
                console.log(error);
            });
        }
        // toggle selection for a given employee by name
        $scope.toggleSelection = function toggleSelection(locations) {
            var idx = $scope.selection.indexOf(locations);

            // is currently selected
            if (idx > -1) {
                $scope.selection.splice(idx, 1);
            }

            // is newly selected
            else {
                $scope.selection.push(locations);
            }
        };


        /**
         * Permission selects
         *
         * @param      {<type>}  code    The code
         */
        $scope.permissionSelection = function permissionSelection(code) {
            var idx = $scope.selectionPermission.indexOf(code);
            if (idx > -1) {
                $scope.selectionPermission.splice(idx, 1);
            } else {
                $scope.selectionPermission.push(code);
            }
        };
        /**
         * Creates an user.
         *
         * @public
         *
         * @memberof   Users Controller
         *
         * @author     jayesh
         *
         * @param      {object}  user    The user
         */
        ViewModel.createUser = function(user) {
            user.permission = $scope.selectionPermission;
            UsersService.createUser(user).then(function(result) {
                $mdDialog.cancel();
                ToastService.success(result.data.message);
                $state.go('app.main.users');
            });
        }

        /**
         * Closes a dialog.
         *
         * @public
         *
         * @memberof   Customer Base Controller
         *
         * @author     jayesh
         */
        ViewModel.closeDialog = function closeDialog() {
            $scope.selection = [];
            DialogService.cancel();
        }

        /**
         * Role changes Pemission
         *
         * @param      {string}  role    The role
         */
        function _RolePermission(role) {
            if (role !== '-1') {
                ViewModel.roleSelected = true;
                if (role == 'SuperAdmin') {
                    ViewModel.rolesAdmin = true;
                    for (i = 0; i < ViewModel.permission.User.length; i++) {
                        $scope.selectionPermission.push(ViewModel.permission.User[i].code);
                        $scope.selectionPermission.push(ViewModel.permission.dashboard[i].code);
                    }
                } else {
                    $scope.selectionPermission = ["rDashboard", "rUsers"];
                    ViewModel.rolesAdmin = false;
                }
            } else {
                $scope.selectionPermission = ["rDashboard", "rUsers"];
                ViewModel.rolesAdmin = false;
                ViewModel.roleSelected = false;
            }
        }
    }
});