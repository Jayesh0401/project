/**
 * @namespace act.Main
 */
define([
    'app/main-my-task/services/myTask.services',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.MyTasks')
        .controller('MainEditMyTaskController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'MyTaskService', '$mdDialog', 'DialogService', '$rootScope', '$scope', '$stateParams'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  TeamsService  act.Main.Project.ProjectsService
     */
    function controller($state, logger, ToastService, MyTaskService, $mdDialog, DialogService, $rootScope, $scope,    $stateParams) {
        $rootScope.pageTitle = 'My Tasks';
        var ViewModel = this;
        var params = $stateParams;
        
        // function Declaration
        ViewModel.GetMyProjectTaskToEdit = _GetMyProjectTaskToEdit;
        ViewModel.submitTask = _submitTask;
        // calling function controller loads
        _GetMyProjectTaskToEdit();

        /**
         * Gets my project task to edit.
         */
        function _GetMyProjectTaskToEdit() {
          var id = params.id;
          MyTaskService.GetMyProjectTaskToEdit({id:id}).then(function(response) {
                if (response.data.success == true && response.data.data) {
                  ViewModel.teamMembers = response.data.data[0].assignedTo;
                  response.data.data[0].start_date = new Date(response.data.data[0].start_date);
                  response.data.data[0].end_date = new Date(response.data.data[0].end_date);
                  ViewModel.MyTask = response.data.data[0];
                }
            }).catch(function(error) {
                console.log(error);
            });
        }
        function _submitTask(data) {
          var myTask = {
            id: data.id,
            status:data.status
          }
          MyTaskService.EditMyProjectTask({data:myTask}).then(function(response) {
                if (response.data.success == true) {
                  ToastService.success(response.data.message);
                  $state.go('app.main.myTask');
                }
            }).catch(function(error) {
                console.log(error);
            }); 
        }    
    }
});