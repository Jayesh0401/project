/**
 * @namespace act.Main
 */
define([
    'app/main-my-task/services/myTask.services',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.MyTasks')
        .controller('MainTaskBaseController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'MyTaskService', '$mdDialog', 'DialogService', '$rootScope', '$scope'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  TeamsService  act.Main.Project.ProjectsService
     */
    function controller($state, logger, ToastService, MyTaskService, $mdDialog, DialogService, $rootScope, $scope) {
        $rootScope.pageTitle = 'My Tasks';
        var ViewModel = this;

        // Parameter Declaration
        ViewModel.MyTasks = [];

        // Function Declaration
        ViewModel.GetMyAllTasks = _GetMyAllTasks;
        ViewModel.EditMyTask = _EditMyTask;

        // Calling Function on page load
        _GetMyAllTasks();

        /**
         * Gets my all tasks.
         */
        function _GetMyAllTasks() {
            MyTaskService.GetMyAllTasks().then(function(response) {
                if (response.data.success == true && response.data.data) {
                    for (i = 0; i < response.data.data[0].myTask.length; i++) {
                        response.data.data[0].myTask[i].end_date = new Date(response.data.data[0].myTask[i].end_date);
                    }
                    console.log(response.data.data[0].myTask.end_date);
                    ViewModel.MyTasks = response.data.data[0].myTask;
                }
            }).catch(function(error) {
                console.log(error);
            });
        }

        function _EditMyTask(taskId) {
          $state.go('app.main.EditMyTask', {id: taskId});
        }
    }
});