define([
    'app-bootstrap'
], function() {

    angular.module('act.Main.MyTasks')
        .factory('MyTaskService', MyTaskService);

    MyTaskService.$inject = ['logger', 'Rest', '$q', '$timeout'];

    function MyTaskService(logger, Rest, $q, $timeout) {
      var log = logger.log().child('MyTaskService');
      return {
      	GetMyAllTasks: GetMyAllTasks,
        GetMyProjectTaskToEdit: GetMyProjectTaskToEdit,
        EditMyProjectTask: EditMyProjectTask
      };

      /**
       * Gets my all tasks.
       *
       * @class      GetMyAllTasks (name)
       * @return     {return}  My all tasks.
       */
      function GetMyAllTasks() {
        var deferred = $q.defer();
      	var httpResource = Rest.resource('get/all/my').post('tasks');
        httpResource().then(function(result) {
          deferred.resolve(result);
        }).catch(function(error) {
          deferred.reject();
        }
        );
        return deferred.promise;
      }

      /**
       * Gets my project task to edit.
       *
       * @class      GetMyProjectTaskToEdit (name)
       * @param      {id}  id      The identifier
       * @return     {return}  My project task to edit.
       */
      function GetMyProjectTaskToEdit(id) {
        var deferred = $q.defer();
        var httpResource = Rest.resource('get/task/to').post('edit');
        httpResource(id).then(function(result) {
          deferred.resolve(result);
        }).catch(function(error) {
          deferred.reject();
        }
        );
        return deferred.promise;
      }

      /**
       * Edits My Project Task
       *
       * @class      EditMyProjectTask (name)
       * @param      {data}  data    The data
       * @return     {return}  { return }
       */
      function EditMyProjectTask(data) {
        var deferred = $q.defer();
        var httpResource = Rest.resource('edit/my/project').post('task');
        httpResource(data).then(function(result) {
          deferred.resolve(result);
        }).catch(function(error) {
          deferred.reject();
        }
        );
        return deferred.promise;
      }
    }
});