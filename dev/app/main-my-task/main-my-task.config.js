/**
 * @namespace act.Users
 */
define([
 'app-bootstrap'
], function() {

  angular
    .module('act.Main.MyTasks')
    .config(RouterConfig);


  // /////////////////////////////////////

  /*=====================================
  =            Config Blocks            =
  =====================================*/

  RouterConfig.$inject = ['APP_BASE_PATH', '$stateProvider', 'lazyProvider'];

  /**
   * Configure the act.Main.Users module's routes
   *
   * @public
   *
   * @memberof   act.Main
   *
   * @author     jayeshactonate
   *
   * @class
   * @param      {String}  APP_BASE_PATH   App Base path
   * @param      {Object}  $stateProvider  ui-router's stateProvider which is used to create
   *                                       application states/routes
   * @param      {Object}  lazyProvider    Provider instance of act.lazy used to lazy load modules
   */
  function RouterConfig(APP_BASE_PATH, $stateProvider, lazyProvider) {

    var BASE_DIR_CTRL = APP_BASE_PATH + 'app/main-my-task/ctrl/';
    var BASE_DIR_TPL = APP_BASE_PATH + 'app/main-my-task/tpl/';

    lazyProvider.configure('act.Main.MyTasks');

    $stateProvider
      .state('app.main.myTask', {
        url: '/my/task',
        templateUrl: BASE_DIR_TPL + 'base.tpl.html',
        controller: 'MainTaskBaseController',
        controllerAs: 'ViewModel',
        resolve: {
          services: ['lazy',
            function(lazy) {
              return lazy.load(BASE_DIR_CTRL + 'base.ctrl.js');
            }
          ]
        }
      })
      .state('app.main.EditMyTask', {
        url: '/edit/my/task/:id',
        templateUrl: BASE_DIR_TPL + 'editMyTask.tpl.html',
        controller: 'MainEditMyTaskController',
        controllerAs: 'ViewModel',
        resolve: {
          services: ['lazy',
            function(lazy) {
              return lazy.load(BASE_DIR_CTRL + 'edit.ctrl.js');
            }
          ]
        }
      });
  }
});
