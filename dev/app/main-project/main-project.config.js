/**
 * @namespace act.Users
 */
define([
 'app-bootstrap'
], function() {

  angular
    .module('act.Main.Project')
    .config(RouterConfig);


  // /////////////////////////////////////

  /*=====================================
  =            Config Blocks            =
  =====================================*/

  RouterConfig.$inject = ['APP_BASE_PATH', '$stateProvider', 'lazyProvider'];

  /**
   * Configure the act.Main.Users module's routes
   *
   * @public
   *
   * @memberof   act.Main
   *
   * @author     jayeshactonate
   *
   * @class
   * @param      {String}  APP_BASE_PATH   App Base path
   * @param      {Object}  $stateProvider  ui-router's stateProvider which is used to create
   *                                       application states/routes
   * @param      {Object}  lazyProvider    Provider instance of act.lazy used to lazy load modules
   */
  function RouterConfig(APP_BASE_PATH, $stateProvider, lazyProvider) {

    var BASE_DIR_CTRL = APP_BASE_PATH + 'app/main-project/ctrl/';
    var BASE_DIR_TPL = APP_BASE_PATH + 'app/main-project/tpl/';

    lazyProvider.configure('act.Main.Projects');

    $stateProvider
      .state('app.main.project', {
        url: '/project',
        templateUrl: BASE_DIR_TPL + 'base.tpl.html',
        controller: 'MainProjectBaseController',
        controllerAs: 'ViewModel',
        resolve: {
          services: ['lazy',
            function(lazy) {
              return lazy.load(BASE_DIR_CTRL + 'base.ctrl.js');
            }
          ]
        }
      }).state('app.main.newProject', {
        url: '/new/project',
        templateUrl: BASE_DIR_TPL + 'newProject.tpl.html',
        controller: 'MainNewProjectController',
        controllerAs: 'ViewModel',
        resolve: {
          services: ['lazy',
            function(lazy) {
              return lazy.load(BASE_DIR_CTRL + 'newProject.ctrl.js');
            }
          ]
        }
      })
      .state('app.main.editProject', {
        url: '/edit/project/:id',
        templateUrl: BASE_DIR_TPL + 'editProject.tpl.html',
        controller: 'MainEditProjectController',
        controllerAs: 'ViewModel',
        resolve: {
          services: ['lazy',
            function(lazy) {
              return lazy.load(BASE_DIR_CTRL + 'editProject.ctrl.js');
            }
          ]
        }
      });
  }

});
