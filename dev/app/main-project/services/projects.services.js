define([
	'app-bootstrap'
],function(){
	
	angular.module('act.Main.Project')
	.factory('ProjectsService', ProjectsService);
	
  ProjectsService.$inject = ['logger', 'Rest', '$q', '$timeout'];
  
  function ProjectsService(logger, Rest, $q, $timeout) {
    var log = logger.log().child('ProjectsService');
    
    return {
      GetProject: GetProject
    };
    

    /**
     * Gets the project.
     *
     * @class      GetProject (name)
     * @return     {return}  The project.
     */
    function GetProject() {
      var deferred = $q.defer();
        var httpResource = Rest.resource('get').post('projects');
        httpResource().then(function(result) {
          deferred.resolve(result);
        }).catch(function(error) {
          deferred.reject();
        }
        );
        return deferred.promise;
    }

  }

});