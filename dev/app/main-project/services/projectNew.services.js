define([
	'app-bootstrap'
],function(){
	
	angular.module('act.Main.Project')
	.factory('ProjectNewService', ProjectNewService);
  ProjectNewService.$inject = ['logger', 'Rest', '$q', '$timeout'];
  
  function ProjectNewService(logger, Rest, $q, $timeout) {
    var log = logger.log().child('ProjectNewService');
    
    return {
      getProjectManager:getProjectManager,
      getTeamLeaders: getTeamLeaders,
      SaveProject: SaveProject
    };

    /**
     * Gets the project manager.
     *
     * @return     {return}  The project manager.
     */
    function getProjectManager() {
      var deferred = $q.defer();
      var httpResource = Rest.resource('project/manager').post('get');
      httpResource().then(function(result) {
        deferred.resolve(result);
      }).catch(function(error) {
        deferred.reject();
      }
      );
      return deferred.promise;
    }

    /**
     * Gets the team leaders.
     *
     * @return     {return}  The team leaders.
     */
    function getTeamLeaders() {
      var deferred = $q.defer();
      var httpResource = Rest.resource('get/team').post('leaders');
      httpResource().then(function(result) {
        deferred.resolve(result);
      }).catch(function(error) {
        deferred.reject();
      }
      );
      return deferred.promise;
    }

    /**
     * Saves a project.
     *
     * @class      SaveProject (name)
     * @param      {object}  data    The data
     */
    function SaveProject(data) {
      var deferred = $q.defer();
      var httpResource = Rest.resource('save').post('project');
      httpResource(data).then(function(result) {
        deferred.resolve(result);
      }).catch(function(error) {
        console.log(error);
        deferred.reject(error);
      });
      return deferred.promise;
    }
  }
});