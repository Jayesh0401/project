define([
    'app-bootstrap'
], function() {

    angular.module('act.Main.Project')
        .factory('ProjectEditService', ProjectEditService);
    ProjectEditService.$inject = ['logger', 'Rest', '$q', '$timeout'];

    function ProjectEditService(logger, Rest, $q, $timeout) {
        var log = logger.log().child('ProjectEditService');

        return {
            getProjectManager: getProjectManager,
            getTeamLeaders: getTeamLeaders,
            getProjectById: getProjectById,
            UpdateProject: UpdateProject
        };

        /**
         * Gets the project manager.
         *
         * @return     {return}  The project manager.
         */
        function getProjectManager() {
            var deferred = $q.defer();
            var httpResource = Rest.resource('project/manager').post('get');
            httpResource().then(function(result) {
                deferred.resolve(result);
            }).catch(function(error) {
                deferred.reject();
            });
            return deferred.promise;
        }

        /**
         * Gets the team leaders.
         *
         * @return     {return}  The team leaders.
         */
        function getTeamLeaders() {
            var deferred = $q.defer();
            var httpResource = Rest.resource('get/team').post('leaders');
            httpResource().then(function(result) {
                deferred.resolve(result);
            }).catch(function(error) {
                deferred.reject();
            });
            return deferred.promise;
        }

        /**
         * Gets the project by identifier.
         *
         * @param      {id}  id      The identifier
         * @return     {return}  The project by identifier.
         */
        function getProjectById(id) {
            var deferred = $q.defer();
            var httpResource = Rest.resource('get/project').post('details');
            httpResource(id).then(function(result) {
                deferred.resolve(result);
            }).catch(function(error) {
                deferred.reject();
            });
            return deferred.promise;
        }


        /**
         * Gets the project by identifier.
         *
         * @param      {id}  id      The identifier
         * @return     {return}  The project by identifier.
         */
        function UpdateProject(project) {
            var deferred = $q.defer();
            var httpResource = Rest.resource('update/project').post('details');
            httpResource(project).then(function(result) {
                deferred.resolve(result);
            }).catch(function(error) {
                deferred.reject();
            });
            return deferred.promise;
        }
    }
});