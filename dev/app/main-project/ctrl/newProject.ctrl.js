/**
 * @namespace act.Main
 */
define([
    'app/main-project/services/projectNew.services',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.Project')
        .controller('MainNewProjectController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'ProjectNewService', '$mdDialog', 'DialogService', '$rootScope', '$scope'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  TeamsService  act.Main.Project.ProjectNewService
     */
    function controller($state, logger, ToastService, ProjectNewService, $mdDialog, DialogService, $rootScope, $scope) {
        $rootScope.pageTitle = 'New Project';
        var ViewModel = this;
        // Parameter Declaration
        ViewModel.project = {};
        ViewModel.TeamLeaders = [];
        ViewModel.Managers = [];
        ViewModel.clients = [];



        // function Declaration
        ViewModel.submitProject = _submitProject;
        ViewModel.getProjectManager = _getProjectManager;
        ViewModel.getTeamLeaders = _getTeamLeaders;
        _getProjectManager();
        _getTeamLeaders();

        ViewModel.clients = [
            {
                'id': '1',
                'name':'John Doe'
            },
            {
                'id': '2',
                'name':'Albert Smith'
            }
        ];
        /**
         * Gets the team leaders.
         */
        function _getTeamLeaders() {
            ProjectNewService.getTeamLeaders().then(function(response) {
               ViewModel.TeamLeaders = response.data.data;
            }).catch(function(error) {

            })
        }
        /**
         * Gets the project manager.
         */
        function _getProjectManager() {
            ProjectNewService.getProjectManager().then(function(response) {
                ViewModel.Managers = response.data.data;
            }).catch(function(error) {
                console.log(error);
            });
        }

        /**
         * Submits the project
         *
         * @param      {object}  project  The project
         */
        function _submitProject(project) {
            ProjectNewService.SaveProject({project:project}).then(function(response) {
                if(response.data.success && response.data.message) {
                  ToastService.success(response.data.message);
                  $state.go('app.main.project');
                }
            }).catch(function(error) {
                console.log(error, '::error');
            })
        }
    }
});