/**
 * @namespace act.Main
 */
define([
    'app/main-project/services/projects.services',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.Project')
        .controller('MainProjectBaseController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'ProjectsService', '$mdDialog', 'DialogService', '$rootScope', '$scope'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  TeamsService  act.Main.Project.ProjectsService
     */
    function controller($state, logger, ToastService, ProjectsService, $mdDialog, DialogService, $rootScope, $scope) {
        $rootScope.pageTitle = 'Projects';
        var ViewModel = this;

        // Parameter Declaration
        

        // Function Declaration
        ViewModel.NewProject = _NewProject;
        ViewModel.getProject = _getProject;
        ViewModel.EditProject = _EditProject;
        _getProject();

        /**
         * Gets the project.
         */
        function _getProject() {
            ProjectsService.GetProject().then(function(response) {
                ViewModel.projects = response.data.data;
            }).catch(function(error) {
                console.log(error);
            });
        }

        /**
         * New Project State
         */
        function _NewProject() {
            $state.go('app.main.newProject');
        }

        /**
         * Edits Project
         *
         * @param      {<type>}  id      The identifier
         */
        function _EditProject(id) {
            $state.go('app.main.editProject', {id:id})
        }
    }
});