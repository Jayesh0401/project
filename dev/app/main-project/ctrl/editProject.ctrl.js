/**
 * @namespace act.Main
 */
define([
    'app/main-project/services/projectEdit.services',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.Project')
        .controller('MainEditProjectController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'ProjectEditService', '$mdDialog', 'DialogService', '$rootScope', '$scope','$stateParams'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  TeamsService  act.Main.Project.ProjectEditService
     */
    function controller($state, logger, ToastService, ProjectEditService, $mdDialog, DialogService, $rootScope, $scope, $stateParams) {
        $rootScope.pageTitle = 'Edit Project';
        var ViewModel = this;
        // Parameter Declaration
        ViewModel.project = {};
        ViewModel.TeamLeaders = [];
        ViewModel.Managers = [];
        ViewModel.clients = [];
        ViewModel.params = $stateParams;
        ViewModel.projectSubmitted = false;


        // function Declaration
        ViewModel.submitProject = _submitProject;
        ViewModel.getProjectManager = _getProjectManager;
        ViewModel.getTeamLeaders = _getTeamLeaders;
        _getProjectManager();
        _getTeamLeaders();

        if(ViewModel.params.id) {
        	var id = ViewModel.params.id;
        	getProjectById(id);
        }
        ViewModel.clients = [
            {
                'id': '1',
                'name':'John Doe'
            },
            {
                'id': '2',
                'name':'Albert Smith'
            }
        ];

        /**
         * Gets the project by identifier.
         *
         * @param      {id}  id      The identifier
         */
        function getProjectById(id) {
        	ProjectEditService.getProjectById({id:id}).then(function(response) {
                var project = response.data.data[0];
                project.start_date = new Date(project.start_date);
                project.end_date = new Date(project.end_date);
                ViewModel.project = project;
                if(ViewModel.project.status === 'SUBMITTED') {
                    ViewModel.projectSubmitted = true;
                }
            }).catch(function(error) {
        		console.log(error);
        	});
        }
        /**
         * Gets the team leaders.
         */
        function _getTeamLeaders() {
            ProjectEditService.getTeamLeaders().then(function(response) {
               ViewModel.TeamLeaders = response.data.data;
            }).catch(function(error) {

            })
        }
        /**
         * Gets the project manager.
         */
        function _getProjectManager() {
            ProjectEditService.getProjectManager().then(function(response) {
                ViewModel.Managers = response.data.data;
            }).catch(function(error) {
                console.log(error);
            });
        }

        /**
         * Submits the project
         *
         * @param      {object}  project  The project
         */
        function _submitProject(project) {
            ProjectEditService.UpdateProject({project:project}).then(function(response) {
                if(response.data.success && response.data.message) {
                  ToastService.success(response.data.message);
                  $state.go('app.main.project');
                }
            }).catch(function(error) {
                console.log(error, '::error');
            })
        }
    }
});