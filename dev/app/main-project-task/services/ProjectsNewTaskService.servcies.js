define([
    'app-bootstrap'
], function() {

    angular.module('act.Main.ProjectTask')
        .factory('ProjectsNewTaskService', ProjectsNewTaskService);

    ProjectsNewTaskService.$inject = ['logger', 'Rest', '$q', '$timeout'];

    function ProjectsNewTaskService(logger, Rest, $q, $timeout) {
        var log = logger.log().child('ProjectsNewTaskService');

        return {
            getProjects: getProjects,
            getProjectDetails: getProjectDetails,
            getTeamMembers: getTeamMembers,
            submitTask: submitTask
        };

        /**
         * Gets the projects.
         *
         * @return     {<type>}  The projects.
         */
        function getProjects() {
            var deferred = $q.defer();
            var httpResource = Rest.resource('get').post('projects');
            httpResource().then(function(result) {
                deferred.resolve(result);
            }).catch(function(error) {
                deferred.reject();
            });
            return deferred.promise;
        }
        /**
         * Gets the project details.
         *
         * @param      {id}  id      The identifier
         * @return     {return}  The project details.
         */
        function getProjectDetails(id) {
            var deferred = $q.defer();
            var httpResource = Rest.resource('get/project').post('details');
            httpResource(id).then(function(result) {
                deferred.resolve(result);
            }).catch(function(error) {
                deferred.reject();
            });
            return deferred.promise;
        }

        /**
         * Gets the team members.
         *
         * @param      {id}  id      The identifier
         * @return     {return}  The team members.
         */
        function getTeamMembers(id) {
          var deferred = $q.defer();
          var httpResource = Rest.resource('get/team').post('members');
          httpResource(id).then(function(result) {
            deferred.resolve(result);
          }).catch(function(error) {
            deferred.reject();
          }
          );
          return deferred.promise;
        }

        function submitTask(task) {
          var deferred = $q.defer();
          var httpResource = Rest.resource('save/project').post('task');
          httpResource(task).then(function(result) {
            deferred.resolve(result);
          }).catch(function(error) {
            deferred.reject();
          }
          );
          return deferred.promise;

        }
    }
});