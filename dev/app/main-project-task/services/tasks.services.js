define([
    'app-bootstrap'
], function() {

    angular.module('act.Main.ProjectTask')
        .factory('ProjectsTaskService', ProjectsTaskService);

    ProjectsTaskService.$inject = ['logger', 'Rest', '$q', '$timeout'];

    function ProjectsTaskService(logger, Rest, $q, $timeout) {
        var log = logger.log().child('ProjectsTaskService');


        
        return {
            getTask: getTask
        };

        /**
         * Gets the task.
         *
         * @return     {return}  The task.
         */
        function getTask() {
        	var deferred = $q.defer();
        	var httpResource = Rest.resource('get').post('tasks');
            httpResource().then(function(result) {
                deferred.resolve(result);
            }).catch(function(error) {
                deferred.reject();
            });
            return deferred.promise;
        }
    }
});