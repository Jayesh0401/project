/**
 * @namespace act.Main
 */
define([
    'app/main-project-task/services/tasks.services',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.ProjectTask')
        .controller('MainProjectTaskBaseController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'ProjectsTaskService', '$mdDialog', 'DialogService', '$rootScope', '$scope'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  TeamsService  act.Main.Project.ProjectsService
     */
    function controller($state, logger, ToastService, ProjectsTaskService, $mdDialog, DialogService, $rootScope, $scope) {
        $rootScope.pageTitle = 'Tasks';
        var ViewModel = this;

        // Parameter Decalaration

        // Function Decalaration
        ViewModel.NewTask = NewTask;
        ViewModel.getTask = _getTask;

        // call function on loading controller
        _getTask();
        /**
         * Gets the task.
         */
        function _getTask() {
            ProjectsTaskService.getTask().then(function(response) {
              ViewModel.tasks = response.data.data;
              console.log(ViewModel.tasks, '::::::::');
              for(i=0;i<ViewModel.tasks.length; i++) {
                var dateString = ViewModel.tasks[i].start_date;
                var dateObj = new Date(dateString);
                var momentObj = moment(dateObj);
                var momentString = momentObj.format('YYYY-MM-DD');
                ViewModel.tasks[i].start_date =momentString;
              }

              for(j=0;j<ViewModel.tasks.length; j++) {
                var dateString = ViewModel.tasks[j].end_date;
                var dateObj = new Date(dateString);
                var momentObj = moment(dateObj);
                var momentString = momentObj.format('YYYY-MM-DD');
                ViewModel.tasks[j].end_date = momentString;
              }
            });
        }

        function NewTask() {
            $state.go('app.main.newProjectTask');
        }
    }
});