/**
 * @namespace act.Main
 */
define([
    'app/main-project-task/services/ProjectsNewTaskService.servcies',
    'app/services/dialog.service'
], function() {

    angular
        .module('act.Main.ProjectTask')
        .controller('MainNewProjectTaskController', controller);

    controller.$inject = ['$state', 'logger', 'ToastService', 'ProjectsNewTaskService','$mdDialog', 'DialogService', '$rootScope', '$scope'];

    return controller;

    // //////////////////////////////////////////////////////

    /**
     * Main Base Controller
     *
     * @public
     *
     * @memberof   act.Main
     *
     * @author     jayeshactonate
     *
     * @param      {Object}  $state        ui-router state service
     * @param      {Object}  logger        act.log logger
     * @param      {Object}  ToastService  act.Services.ToastService
     * @param      {Object}  TeamsService  act.Main.Project.ProjectsService
     */
    function controller($state, logger, ToastService, ProjectsNewTaskService,$mdDialog, DialogService, $rootScope, $scope) {
        $rootScope.pageTitle = 'New Task';
        
        var ViewModel = this;
        ViewModel.checkInMinDate = new Date();
        // Parameter Decalaration

        // Function Decalaration
        ViewModel.getProjects = _getProjects;
        ViewModel.ProjectSelected = _ProjectSelected;
        ViewModel.submitTask = _submitTask;
        // Calling function on controller load
        _getProjects();
        /**
         * Gets the projects.
         */
        function _getProjects() {
            ProjectsNewTaskService.getProjects().then(function(response) {
              ViewModel.projects = response.data.data;
            });
        }

        /**
         * Projects Details
         *
         * @param      {<type>}  id      The identifier
         */
        function _ProjectSelected(id) {
          ProjectsNewTaskService.getProjectDetails({id:id}).then(function(response) {
            if(response.data.success && response.data.data) {
              ViewModel.task.manager =response.data.data[0].manager[0].name;
              ViewModel.task.teamLeader =response.data.data[0].teamLeader[0].name;
              var id = response.data.data[0].teamLeader[0].id;
             ViewModel.projectName = response.data.data[0].name;
              getTeamMembers(id);
            }
            
          }).catch(function(error) {
            console.log(error);
          });
        }

        /**
         * Gets the team members.
         *
         * @param      {id}  id      The identifier
         */
        function getTeamMembers(id) {
         ProjectsNewTaskService.getTeamMembers({id:id}).then(function(response) {
            
            if(response.data.success && response.data.data) {
              ViewModel.teamMembers = response.data.data[0].teamMemberId;
            }
            
          }).catch(function(error) {
            console.log(error);
          }); 
        }

        /**
         * Submits Task
         *
         * @param      {task}  task    The task
         */
        function _submitTask(task) {
          task.projectName = ViewModel.projectName;
          ProjectsNewTaskService.submitTask({task:task}).then(function(response) {
            if(response.data.success) {
              ToastService.success(response.data.message);
              $state.go('app.main.projectTask');
            }
          });
        }
    }
});