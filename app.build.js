/*
    RequireJS Configuration, one file to rule them all.
*/
requirejs.config({
  urlArgs: 'ver=\'0.0.1\'',
  // @if NODE_ENV=='PRODUCTION'
  baseUrl: '/* @echo buildPath */',
  // @endif
  waitSeconds: 0,
  paths: {
    'angular': 'packages/' + 'angular/angular.min',
    'angular-animate': 'packages/' + 'angular-animate/angular-animate.min',
    'angular-aria': 'packages/' + 'angular-aria/angular-aria.min',
    'angular-i18n': 'packages/' + 'angular-i18n/angular-locale_en-in',
    'angular-material': 'packages/' + 'angular-material/angular-material.min',
    'angular-material-icons':'packages/'+'angular-material-icons/angular-material-icons',
    'angular-material-data-table': 'packages/' + 'angular-material-data-table/dist/md-data-table',
    'angular-ui-router': 'packages/' + 'angular-ui-router/release/angular-ui-router.min',
    'debug': 'packages/' + 'visionmedia-debug/dist/debug',
    'oclazyload': 'packages/' + 'oclazyload/dist/ocLazyLoad.require.min',
    'ng-file-upload': 'packages/' + 'ng-file-upload/ng-file-upload.min',
    'angular-chart':'packages'+ '/angular-chart/angular-chart.min',
    'chart': 'packages'+ '/chart.js/dist/Chart.min',
    'c3': 'packages'+ '/c3/c3',
    'd3': 'packages'+ '/d3/d3',
    'moment': 'packages' + '/moment/min/moment.min',
    'interact': 'packages' + '/interactjs/dist/interact.min',
    'interact-map': 'packages' + '/interactjs/dist/interact.min.js.map',
    'angular-bootstrap-calendar': 'packages' + '/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min',
    'lodash': 'packages' + '/lodash/dist/lodash.min',
    'permission':'packages' + '/angular-permission/dist/angular-permission.min',
    'permission-ui':'packages/angular-permission/dist/angular-permission-ui',
    'tinymce': 'packages' + '/tinymce/tinymce',
    'tinymce-ui': 'packages' + '/angular-ui-tinymce/src/tinymce',
    'jquery': 'packages' + '/jquery/dist/jquery.min',
    'sideNav': 'packages/angular-material-sidenav/angular-material-sidenav',

    // Common Modules
    'act-lazy': 'common/act-lazy/main',
    'act-log': 'common/act-log/main',
    'act-rest': 'common/act-rest/main',


    // Application Modules
    'app-bootstrap': 'app/app-bootstrap',
    'main': 'app/main'
  },
  shim: {
    'angular': {
      deps: [],
      exports: 'angular'
    },
    'angular-animate': ['angular'],
    'angular-ui-router': ['angular'],
    'angular-aria': ['angular'],
    'angular-i18n': ['angular'],
    'angular-material': ['angular', 'angular-aria'],
    'angular-material-icons':['angular'],
    'angular-material-data-table': ['angular', 'angular-material'],
    'oclazyload': ['angular'],
    'ng-file-upload': ['angular'],    
    'moment': [],   
    'lodash': ['angular'],
    'interact': [],
    'angular-bootstrap-calendar': ['angular'],
    'permission':['angular'],
    'permission-ui': ['angular', 'angular-ui-router','permission'],
    'tinymce': ['jquery'],
    'tinymce-ui': ['tinymce'],
    'sideNav': ['angular','angular-ui-router'],

    // ACT Modules
    'act-lazy': ['oclazyload'],
    'act-log': ['angular', 'debug'],
    'act-rest': ['angular'],
    'c3': ['angular'],
    'd3': ['angular'],
    'chart': ['angular'],
    'angular-chart': ['angular','c3','d3','chart'],

    // Application Modules
    'app-bootstrap': ['angular-ui-router'],

    // Main Module
    'main': [
      // 3rd party
      'angular',
      'angular-i18n',
      'angular-material',
      'angular-animate',
      'angular-aria',
      'angular-material-icons',
      'angular-material-data-table',
      'permission',
      'permission-ui',
      'tinymce-ui',
      'sideNav'
    ]
  }
});

require([
    'moment',
    'lodash',
    'main'
  ],
  function(moment) {

    window.moment = moment;
    
    angular.bootstrap(document, ['act'], {
      strictDi: true
    });
  }
);